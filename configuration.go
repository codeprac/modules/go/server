package server

import (
	"net/http"

	"github.com/usvc/go-config"
)

const (
	KeyCORSAllowedMethods = "server-cors-allowed-methods"
	KeyCORSAllowedOrigins = "server-cors-allowed-origins"
	KeyInterface          = "server-interface"
	KeyPort               = "server-port"
	KeyPortGRPC           = "server-port-grpc"

	DefaultInterface = "0.0.0.0"
	DefaultPort      = 5476
	DefaultPortGRPC  = 5477
)

var (
	DefaultCORSAllowedMethods = []string{http.MethodGet, http.MethodPost, http.MethodPut}
	DefaultCORSAllowedOrigins = []string{"http://localhost"}
)

func GetConfiguration() config.Map {
	return config.Map{
		KeyCORSAllowedMethods: &config.StringSlice{
			Default: DefaultCORSAllowedMethods,
			Usage:   "comma-delimited list of http methods to allow for the Access-Control-Allow-Methods header",
		},
		KeyCORSAllowedOrigins: &config.StringSlice{
			Default: DefaultCORSAllowedOrigins,
			Usage:   "comma-delimited list of urls to allow for the Access-Control-Allow-Origin header",
		},
		KeyInterface: &config.String{
			Default: DefaultInterface,
			Usage:   "network interface to bind to",
		},
		KeyPort: &config.Int{
			Default: DefaultPort,
			Usage:   "network port for the server to listen on",
		},
		KeyPortGRPC: &config.Int{
			Default: DefaultPortGRPC,
			Usage:   "network port for the grpc server to listen on",
		},
	}
}
