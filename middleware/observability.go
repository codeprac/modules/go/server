package middleware

import (
	"net/http"
	"time"
)

type Metrics struct {
	RequestMethod string
	RequestPath   string
	ResponseCode  int
}
type FormattedLogger func(string, ...interface{})
type MetricsRecorder func(Metrics)
type ResponseWriter struct {
	http.ResponseWriter
	ResponseSize int
	Status       int
}

func (rw ResponseWriter) Header() http.Header {
	return rw.ResponseWriter.Header()
}

func (rw *ResponseWriter) Write(output []byte) (int, error) {
	rw.ResponseSize = len(output)
	return rw.ResponseWriter.Write(output)
}

func (rw *ResponseWriter) WriteHeader(status int) {
	rw.Status = status
	rw.ResponseWriter.WriteHeader(status)
}

type AddObservabilityOpts struct {
	Logger   FormattedLogger
	Recorder MetricsRecorder
}

func AddObservability(to http.Handler, opts AddObservabilityOpts) http.Handler {
	return observe(to, opts)
}

func observe(to http.Handler, opts AddObservabilityOpts) http.HandlerFunc {
	writeLog := opts.Logger
	writeMetrics := opts.Recorder
	return func(w http.ResponseWriter, r *http.Request) {
		timeReceived := time.Now()
		wrappedResponseWriter := &ResponseWriter{ResponseWriter: w}
		to.ServeHTTP(wrappedResponseWriter, r)
		responseDuration := time.Now().Sub(timeReceived).Milliseconds()
		responseSize := wrappedResponseWriter.ResponseSize
		responseStatus := wrappedResponseWriter.Status
		if opts.Logger != nil {
			writeLog("%s - %s \"%s %s\" %v %v \"%s\" \"%s\" rt=%vms",
				r.RemoteAddr,
				r.URL.User,
				r.Method,
				r.URL.Path,
				responseStatus,
				responseSize,
				r.Referer(),
				r.UserAgent(),
				responseDuration,
			)
		}
		if opts.Recorder != nil {
			writeMetrics(Metrics{
				RequestMethod: r.Method,
				RequestPath:   r.URL.Path,
				ResponseCode:  responseStatus,
			})
		}
	}
}
