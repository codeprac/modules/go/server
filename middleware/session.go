package middleware

import (
	"fmt"
	"net/http"
	"strings"
)

type AddSessionContextOpts struct {
	CookieID             string
	ErrorLogger          func(string, ...interface{})
	SessionTokenConsumer func(string) (map[string]interface{}, error)
	SessionTokenProvider func(interface{}, *http.Request) *http.Request
}

func (o AddSessionContextOpts) Validate() error {
	errors := []string{}

	if len(o.CookieID) == 0 {
		errors = append(errors, "missing cookie id")
	}

	if o.SessionTokenConsumer == nil {
		errors = append(errors, "missing token parser")
	}

	if len(errors) > 0 {
		return fmt.Errorf("failed to validate options: ['%s']", strings.Join(errors, "', '"))
	}

	return nil
}

func AddSessionContext(to http.Handler, opts AddSessionContextOpts) (http.Handler, error) {
	if err := opts.Validate(); err != nil {
		return nil, fmt.Errorf("failed to parse options: %s", err)
	}
	parseToken := opts.SessionTokenConsumer
	insertToken := opts.SessionTokenProvider
	errorLog := opts.ErrorLogger
	if errorLog == nil {
		errorLog = func(_ string, _ ...interface{}) {}
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookies := r.Cookies()
		// loop incase there were two cookies of the same name set - this ensures that
		// all cookies of the same name are evaluated and the first successfully parsed
		// one gets injected
		for _, cookie := range cookies {
			if cookie.Name == opts.CookieID && len(cookie.Value) > 0 {
				if tokenData, err := parseToken(cookie.Value); err != nil {
					errorLog("failed to parse cookie['%s'] with value '%s': %s", cookie.Name, cookie.Value, err)
				} else if tokenData != nil {
					r = insertToken(tokenData, r)
					break
				}
			}
		}
		to.ServeHTTP(w, r)
	}), nil
}
