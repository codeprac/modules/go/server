package server

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const DefaultHttpMetricsPath = "/metrics"

type NewHttpMetricsPrometheusOpts struct {
	Path string `json:"path"`
}

func NewHttpMetricsPrometheus(opts ...NewHttpMetricsPrometheusOpts) *HttpMetricsPrometheus {
	path := DefaultHttpMetricsPath
	if len(opts) > 0 && len(opts[0].Path) > 0 {
		path = opts[0].Path
	}

	return &HttpMetricsPrometheus{path: path}
}

type HttpMetricsPrometheus struct {
	path string
}

func (m HttpMetricsPrometheus) GetHttpPath() string {
	return m.path
}

func (m HttpMetricsPrometheus) GetHttpHandlerFunc() http.HandlerFunc {
	return promhttp.Handler().ServeHTTP
}
