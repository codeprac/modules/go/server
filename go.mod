module gitlab.com/codeprac/modules/go/server

go 1.16

require (
	github.com/prometheus/client_golang v0.9.3
	github.com/rs/cors v1.8.0
	github.com/stretchr/testify v1.7.0
	github.com/usvc/go-config v0.4.1
	google.golang.org/grpc v1.21.1
)
