# Server module for Go

This module provides a component that should fulfil the requirements of having a server.

- [Server module for Go](#server-module-for-go)
  - [Integrating configuration](#integrating-configuration)
  - [Initialisation](#initialisation)
    - [Initialising an HTTP server](#initialising-an-http-server)
    - [Initialising a gRPC server](#initialising-a-grpc-server)
  - [Observability](#observability)
    - [Implementing healthcheck endpoints](#implementing-healthcheck-endpoints)
    - [Implementing metrics endpoints](#implementing-metrics-endpoints)
- [Changelog](#changelog)
- [License](#license)

## Integrating configuration

Configuration assumes use of the `usvc/go-config` configuration package.

```go
// ...

import (
  // ...
	"gitlab.com/codeprac/modules/go/server"
  // ...
)

var conf = config.Map{}

var configSets = []config.Map{
	// ...
  server.GetConfiguration(),
  // ...
}

func init() {
  for _, configSet := range configSets {
		for key, value := range configSet {
			conf[key] = value
		}
	}
}
```

## Initialisation

### Initialising an HTTP server

This usage example assumes that the configuration has been integrated as documented above.

```go
// ...

import (
  // ...
	"gitlab.com/codeprac/modules/go/server"
  // ...
)

httpServerOpts := server.NewHTTPServerOptions{
  Interface:      conf.GetString(server.KeyInterface),
  Port:           uint16(conf.GetInt(server.KeyPort)),
  // if the following "Allowed*" are not set, CORS is turned off 
  AllowedMethods: conf.GetStringSlice(server.KeyCORSAllowedMethods),
  AllowedOrigins: conf.GetStringSlice(server.KeyCORSAllowedOrigins),
}
httpServerInstance := server.NewHTTPServer(httpServerOpts)
httpServerInstance.ErrorLogger = func(format string, args ...interface{}) {
  fmt.Printf("ERROR: %s", fmt.Sprintf(format, args...))
}
httpServerInstance.RequestLogger = func(format string, args ...interface{}) {
  fmt.Printf("INCOMING: %s", fmt.Sprintf(format, args...))
}

// api is an arbitrary package exposing the GetHttpHandler()
// method which should return a http.Handler
handler := api.GetHttpHandler()

// start server
if err := httpServerInstance.Start(handler); err != nil {
  panic(err)
}
```

### Initialising a gRPC server

```go
// ...

import (
  // ...
	"gitlab.com/codeprac/modules/go/server"
  // ...
)

grpcServerOpts := server.NewGrpcServerOpts{
  Interface: conf.GetString(server.KeyInterface),
  Port:      uint16(conf.GetInt(server.KeyPort)) + 1,
}
grpcServerInstance := server.NewGrpcServer(grpcServerOpts)

// register handlers (api is an arbitrary gRPC component implementation)
api.Register(grpcServerInstance.Server)

// if reflection is desired, set the flag
grpcServerInstance.IsReflectionEnabled = true

// start server
if err := grpcServerInstance.Start(); err != nil {
  panic(err)
}

```

## Observability

### Implementing healthcheck endpoints

> Custom endpoints need to implement the `server.HttpHandler` interface by having the methods `GetHttpPath()` which returns a `string`, and `GetHttpHandlerFunc()` which returns a `http.HandlerFunc`.

The following example demonstrates fully-featured creations of a `server.HttpHealthcheck` and assigning the probe to the liveness probe handler. Note that the `Checks` property is *optional*. 

```go
// ...

import (
  // ...
  "gitlab.com/codeprac/modules/go/server"
  // ...
)

livenessProbe := server.NewHttpHealthcheckOpts{
  Checks: []server.HttpProbeHandler{
    func(r http.Reuqest) error {
      // this indicates a check that always passes
      return nil
    },
    func(r http.Reuqest) error {
      // this indicates a check that always fails
      return errors.New("always fails")
    },
  },
  Path: "/liveness",
}
serverInstance.LivenessProbeHandler = server.NewHttpHealthcheck(livenessProbe)
```

### Implementing metrics endpoints

There is built-in support for `promhttp`. An example metrics endpoint using the `promhttp` package follows:

```go
// ...

import (
  // ...
  "github.com/prometheus/client_golang/prometheus"
  "gitlab.com/codeprac/modules/go/server/middleware"
  // ...
)

// implement a metrics recorder
endpointCallCount := *prometheus.NewCounterVec(prometheus.CounterOpts{
				Name: "http_requests_total",
				Help: "Number of requests",
			}, []string{"method", "path", "code"})
			prometheus.Register(endpointCallCount)
httpServerInstance.MetricsRecorder = func(m middleware.Metrics) {
  endpointCallCount.WithLabelValues(
    m.RequestMethod,
    m.RequestPath,
    strconv.Itoa(m.ResponseCode),
  ).Add(1)
}

// register the promhttp metrics endpoint
metricsOpts := server.NewHttpMetricsPrometheusOpts{
  Path: DefaultMetricsPath,
}
serverInstance.MetricsHandler = server.NewHttpMetricsPrometheus(metricsOpts)
```

# Changelog

| Version tag | Description                                                                         |
| ----------- | ----------------------------------------------------------------------------------- |
| v0.9.0      | Added in-built `reflection` support for gRPC servers via `IsReflectionEnabled` flag |
| v0.8.0      | Added support for gRPC server creation                                              |
| v0.6.0      | Added `responder` subpackage for handling responding                                |
| v0.5.0      | Added in-built support for using `promhttp`                                         |
| v0.4.0      | Added observability support for using a custom metrics recorder                     |
| v0.3.0      | Added in-built support for creating custom healthchecks                             |
| v0.2.0      | Merged `serverconfiguration` subpackage into the main `server` package              |
| v0.1.0      | Initial release for `server` package                                                |

# License

Code is licensed under The MIT License. [See full license](./LICENSE).
