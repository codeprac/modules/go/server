package server

import (
	"fmt"
	"net"
	"strconv"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type NewGrpcServerOpts struct {
	Interface string
	Port      uint16
}

func NewGrpcServer(opts NewGrpcServerOpts) *GrpcServer {
	addr := net.JoinHostPort(opts.Interface, strconv.FormatUint(uint64(opts.Port), 10))
	serverOpts := []grpc.ServerOption{}
	return &GrpcServer{
		Address: addr,
		Options: serverOpts,
		Server:  grpc.NewServer(serverOpts...),
	}
}

type GrpcServer struct {
	*grpc.Server
	Address             string
	IsReflectionEnabled bool
	Options             []grpc.ServerOption
}

func (s *GrpcServer) Close() {
	s.Server.GracefulStop()
}

func (s *GrpcServer) Start() error {
	if s.IsReflectionEnabled {
		reflection.Register(s.Server)
	}
	listener, err := net.Listen("tcp", s.Address)
	if err != nil {
		return fmt.Errorf("failed to open a tcp listener at %s", s.Address)
	}
	return s.Server.Serve(listener)
}
