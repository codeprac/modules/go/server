package responder

import (
	"net/http"
)

type HTTP struct {
	Source      string
	ErrorLogger LogAs
}

func (r HTTP) BadRequest(code string, data ...interface{}) HttpResponse {
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: httpResponse{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusBadRequest,
		},
	}
}

func (r HTTP) Forbidden(code string, data ...interface{}) HttpResponse {
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: httpResponse{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusForbidden,
		},
	}
}

func (r HTTP) InternalServerError(code string, data ...interface{}) HttpResponse {
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: httpResponse{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusInternalServerError,
		},
	}
}

func (r HTTP) NotImplemented(code string, data ...interface{}) HttpResponse {
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: httpResponse{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusNotImplemented,
		},
	}
}

func (r HTTP) NotFound(code string, data ...interface{}) HttpResponse {
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: httpResponse{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusNotFound,
		},
	}
}

func (r HTTP) OK(data ...interface{}) HttpResponse {
	code := "ok"
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: httpResponse{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusOK,
		},
	}
}

func (r HTTP) TemporaryRedirect(toThisURL string) HttpResponse {
	return HttpResponse{
		Source: r.Source,
		instance: httpResponse{
			Headers: map[string][]string{
				"Location": []string{toThisURL},
			},
			HTTPCode: http.StatusTemporaryRedirect,
			IsEmpty:  true,
		},
	}
}

func (r HTTP) Unauthorized(code string, data ...interface{}) HttpResponse {
	var d interface{}
	if len(data) > 0 {
		d = data[0]
	}
	return HttpResponse{
		Source: r.Source,
		instance: httpResponse{
			Code:     &code,
			Data:     &d,
			HTTPCode: http.StatusUnauthorized,
		},
	}
}
