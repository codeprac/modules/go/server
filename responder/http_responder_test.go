package responder

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"
)

type HttpTests struct {
	suite.Suite
}

func TestHttp(t *testing.T) {
	suite.Run(t, &HttpTests{})
}

func (s HttpTests) Test_BadRequest() {
	w := httptest.NewRecorder()
	HTTP{Source: "module"}.BadRequest("code", "data").Send(w)
	expectedBody := `{"code":"code","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusBadRequest, w.Result().StatusCode)
}

func (s HttpTests) Test_Forbidden() {
	w := httptest.NewRecorder()
	HTTP{Source: "module"}.Forbidden("code", "data").Send(w)
	expectedBody := `{"code":"code","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusForbidden, w.Result().StatusCode)
}

func (s HttpTests) Test_InternalServerError() {
	w := httptest.NewRecorder()
	HTTP{Source: "module"}.InternalServerError("code", "data").Send(w)
	expectedBody := `{"code":"code","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusInternalServerError, w.Result().StatusCode)
}

func (s HttpTests) Test_NotImplemented() {
	w := httptest.NewRecorder()
	HTTP{Source: "module"}.NotImplemented("code", "data").Send(w)
	expectedBody := `{"code":"code","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusNotImplemented, w.Result().StatusCode)
}

func (s HttpTests) Test_NotFound() {
	w := httptest.NewRecorder()
	HTTP{Source: "module"}.NotFound("code", "data").Send(w)
	expectedBody := `{"code":"code","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusNotFound, w.Result().StatusCode)
}

func (s HttpTests) Test_OK() {
	w := httptest.NewRecorder()
	HTTP{Source: "module"}.OK("data").Send(w)
	expectedBody := `{"code":"ok","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusOK, w.Result().StatusCode)
}

func (s HttpTests) Test_TemporaryRedirect() {
	expectedURL := "http://localhost:3000"
	w := httptest.NewRecorder()
	HTTP{Source: "module"}.TemporaryRedirect(expectedURL).Send(w)
	expectedBody := ""
	s.Equal(expectedBody, w.Body.String())
	s.Contains(w.Result().Header["Location"], expectedURL)
	s.Equal(http.StatusTemporaryRedirect, w.Result().StatusCode)
}

func (s HttpTests) Test_Unauthorized() {
	w := httptest.NewRecorder()
	HTTP{Source: "module"}.Unauthorized("code", "data").Send(w)
	expectedBody := `{"code":"code","data":"data"}`
	s.Equal(expectedBody, w.Body.String())
	s.Equal(http.StatusUnauthorized, w.Result().StatusCode)
}
