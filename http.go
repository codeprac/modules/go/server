package server

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/codeprac/modules/go/server/middleware"
)

type NewHTTPServerOptions struct {
	Interface      string
	Port           uint16
	MaxHeaderBytes int
	AllowedMethods []string
	AllowedOrigins []string
}

func NewHTTPServer(opts NewHTTPServerOptions) *HTTPServer {
	address := fmt.Sprintf("%s:%v", opts.Interface, opts.Port)
	return &HTTPServer{
		Server: http.Server{
			Addr:              address,
			MaxHeaderBytes:    opts.MaxHeaderBytes,
			IdleTimeout:       10 * time.Second,
			ReadTimeout:       10 * time.Second,
			ReadHeaderTimeout: 10 * time.Second,
			WriteTimeout:      10 * time.Second,
		},
		CORSOptions: &HTTPCORSOptions{
			AllowedMethods:   opts.AllowedMethods,
			AllowedOrigins:   opts.AllowedOrigins,
			AllowCredentials: true,
		},
	}
}

type HttpHandler interface {
	GetHttpPath() string
	GetHttpHandlerFunc() http.HandlerFunc
}

type HTTPCORSOptions struct {
	AllowedMethods   []string
	AllowedOrigins   []string
	AllowCredentials bool
}

type HTTPServer struct {
	http.Server

	// CookieID specifies the name of the cookie that identifies session data, if
	// this is not set, session management will be disabled
	CookieID string

	// CORSOptions specifies the corss-origin-resource-sharing configuration
	// for this server instance, if this is not set, CORS will be disabled
	CORSOptions *HTTPCORSOptions

	// ErrorLogger is a log function that is used when an error occurs
	ErrorLogger func(string, ...interface{})

	// MetricsHandler defines the handler for metrics scraping, if this is not set,
	// there will be no metrics endpoint even if MetricsRecorder is defined
	MetricsHandler HttpHandler

	// MetricsRecorder defines the function that should add metrics for an endpoint,
	// if this is not set, metrics will not be sent to your own metrics recording
	// code
	MetricsRecorder middleware.MetricsRecorder

	// LivenessProbeHandler defines the handling of liveness checks, if this is
	// not set, there will be no liveness check endpoint
	//
	// Reference link: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-a-liveness-http-request
	LivenessProbeHandler HttpHandler

	// ReadinessProbeHandler defines the handling of readiness checks, if this is
	// not set, there will be no readiness check endpoint
	//
	// Reference link: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-readiness-probes
	ReadinessProbeHandler HttpHandler

	// RequestLogger is a log function that is used for logging requests information,
	// if this is not set, request logs will not be created. Request logs are done in
	// the nginx format
	RequestLogger func(string, ...interface{})

	// TokenDataProvider returns the token data given a string-based token, when
	// not defined, the session management middleware will not be added
	TokenDataProvider func(string) (map[string]interface{}, error)

	// TokenDataInserter adds the provided token data into the request context, when
	// not defined, the session management middleware will not be added
	TokenDataInserter func(interface{}, *http.Request) *http.Request
}

func (s *HTTPServer) Start(handler http.Handler) error {
	mux := http.NewServeMux()
	if s.MetricsHandler != nil {
		mux.Handle(s.MetricsHandler.GetHttpPath(), s.MetricsHandler.GetHttpHandlerFunc())
	}
	if s.LivenessProbeHandler != nil {
		mux.Handle(s.LivenessProbeHandler.GetHttpPath(), s.LivenessProbeHandler.GetHttpHandlerFunc())
	}
	if s.ReadinessProbeHandler != nil {
		mux.Handle(s.ReadinessProbeHandler.GetHttpPath(), s.ReadinessProbeHandler.GetHttpHandlerFunc())
	}
	mux.Handle("/", handler)

	var primedHandler http.Handler = mux

	if s.CORSOptions != nil {
		primedHandler = middleware.AddCORS(primedHandler, middleware.AddCORSOpts{
			AllowedMethods:   s.CORSOptions.AllowedMethods,
			AllowedOrigins:   s.CORSOptions.AllowedOrigins,
			AllowCredentials: s.CORSOptions.AllowCredentials,
		})
	}

	if len(s.CookieID) > 0 && s.TokenDataProvider != nil && s.TokenDataInserter != nil {
		var err error
		primedHandler, err = middleware.AddSessionContext(primedHandler, middleware.AddSessionContextOpts{
			CookieID:             s.CookieID,
			ErrorLogger:          s.ErrorLogger,
			SessionTokenConsumer: s.TokenDataProvider,
			SessionTokenProvider: s.TokenDataInserter,
		})
		if err != nil {
			return fmt.Errorf("failed to attach session context middleware: %s", err)
		}
	}

	observabilityOpts := middleware.AddObservabilityOpts{}
	if s.RequestLogger != nil {
		observabilityOpts.Logger = middleware.FormattedLogger(s.RequestLogger)
	}
	if s.MetricsRecorder != nil {
		observabilityOpts.Recorder = s.MetricsRecorder
	}
	primedHandler = middleware.AddObservability(primedHandler, observabilityOpts)
	s.Server.Handler = primedHandler
	return s.Server.ListenAndServe()
}
